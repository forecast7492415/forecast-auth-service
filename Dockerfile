FROM node:14
RUN mkdir -p /app
WORKDIR /app
COPY . .
# RUN npm rebuild bcrypt
RUN npm install
RUN npm install -g sequelize-cli
CMD ["sh", "-c", "npx sequelize-cli db:migrate && node index.js"]
