'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Station extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.StationType, {foreignKey: 'type', as: 'StationType'});
      this.belongsToMany(models.User, {
        through: models.UserStation, 
        as: 'user',
        foreignKey: 'station'
      })
    }
  }
  Station.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    name: DataTypes.JSON,
    type: DataTypes.UUID,
    capacity: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Station',
  });
  return Station;
};