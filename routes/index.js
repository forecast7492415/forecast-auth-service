const express = require('express')
const router = express.Router();
const stationTypeController = require('../controllers/stationtype');
const stationController = require('../controllers/station');
const userController = require('../controllers/user');
const {isAuth, isAdmin} = require('../middleware');

router.post('/register-user', isAuth, isAdmin, userController.registerUser);
router.post('/register-admin', userController.registerAdmin);
router.post('/sign-in', userController.loginUser);
router.post('/access-token', userController.getAccessToken);
router.post('/login-admin', userController.loginAdmin);
router.post('/station-types', isAuth, isAdmin, stationTypeController.createStationType);
router.get('/station-types/:id', isAuth, stationTypeController.getStationTypeById);
router.get('/station-types', isAuth, isAdmin, stationTypeController.getAllStationTypes);
router.post('/stations', isAuth, isAdmin, stationController.createStation);
router.get('/stations/:id', isAuth, stationController.getStationById);
router.post('/stations/:id', isAuth, isAdmin, stationController.addUserToStation);

module.exports = router;