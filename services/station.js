const { Station, User, UserStation } = require('../models');
const CustomError = require('../utils/custom_error');
const stationTypeService = require('./stationtype');

class StationService { 
    async createStation(body) {
        const {name, type, capacity} = body;
        const station = await Station.create({name, type, capacity});
        station.type = await stationTypeService.getStationTypeById(type);
        return station;
    }

    async getStationById(id) {
        const station = await Station.findOne({where : {id}});
        station.type = await stationTypeService.getStationTypeById(station.type);
        return {
            id: station.id,
            name: station.name,
            type: station.type,
            capacity: station.capacity,
            tariff: 30
        };
    }

    async addUserToStation(userId, stationId) {
        if (!userId) throw new CustomError("No query param field 'user' provided");
        if (!await User.findOne({ where: {id: userId} })) throw new CustomError("No user with such an id", 400);
        if (!await Station.findOne({ where: {id: stationId} })) throw new CustomError("No station with such an id", 400);
        if (await UserStation.findOne({
            where: {
                user: userId,
                station: stationId
            }
        })) throw new CustomError("User is already added to this station", 400);

        await UserStation.create({
            user: userId,
            station: stationId
        });
        
        return {
            message: 'User added to this station'
        };
    }

    async getUsersStation(userId) {
        const userStations = await UserStation.findAll({ where: {user: userId} });
        const stations = await Promise.all(userStations.map(async userStation => {
            const station = await this.getStationById(userStation.station);
            return station;
        }));
        return stations;
    }
}

const stationService = new StationService();

module.exports = stationService;