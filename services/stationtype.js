const { StationType } = require('../models');

class StationTypeService {
    async createStationType(body) {
        const { name } = body;
        const stationType = await StationType.create({name});
        return stationType;
    }
    async getStationTypeById(id) {
        const stationType = await StationType.findOne({where: {id}});
        return {
            id: stationType.id,
            name: stationType.name
        };
    }
    async getAllStationTypes() {
        const stationTypes = await StationType.findAll({
            attributes: ['id', 'name']
        });
        return stationTypes;
    }
}

const stationTypeService = new StationTypeService();

module.exports = stationTypeService;