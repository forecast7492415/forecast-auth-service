const { User } = require('../models');
const stationService = require('./station');
const bcrypt = require('bcrypt')
const validator = require('validator');
const {SECRET_ACCESS_KEY, SECRET_REFRESH_KEY} = require('../config/config');
const jwt = require('jsonwebtoken');
const CustomError = require('../utils/custom_error');

class UserService {
    async registerUser(body, isAdmin) {
        const {name, password, phone, email} = body;

        if (!name || !password || !phone || !email) throw new Error("Some of the fields is not provided");

        if (!validator.isEmail(email)) throw new Error("Email is not valid");
        if (!validator.isMobilePhone(phone)) throw new Error("Phone is not valid");

        if (await User.findOne({where : {email}})) throw new Error("User with such an email exists");
        if (await User.findOne({where : {phone}})) throw new Error("User with such a phone exists");

        const hashedPassword = await this.#hashPassword(password);

        await User.create({
            name,
            password: hashedPassword,
            phone,
            email,
            is_admin: isAdmin
        });

        return {
            message: 'User successfully created'
        };
    }

    async loginUser(body, isAdmin) {
        const {email, password} = body;
        
        if (!email || !password ) throw new CustomError("Some of the fields is not provided", 400);

        const user = await User.findOne({where : { email } });

        if(!user) throw new CustomError('No user with such an email exists', 400);

        const passwordMatch = await this.#checkPassword(password, user.password);

        if (!passwordMatch) throw new CustomError('Email or password is incorrect', 400);

        if ((user.is_admin && !isAdmin) || (!user.is_admin && isAdmin)) throw new CustomError('No permission', 403);

        const tokenBody = {
            id: user.id,
            phone: user.phone,
            email: user.email,
            is_admin: user.is_admin,
            name: user.name
        };

        const accessToken = this.#generateToken(tokenBody, SECRET_ACCESS_KEY);
        const refreshToken = this.#generateToken(tokenBody, SECRET_REFRESH_KEY);
        
        const stations = await stationService.getUsersStation(user.id);

        return {
            access_token: accessToken,
            refresh_token: refreshToken,
            user: {
                id: user.id,
                email: user.email,
                stations,
                role: "admin"
            }
        };
    }

    async getAccessToken(body) {
        let refreshToken = body.refresh_token;
        if (!refreshToken) throw new CustomError("No token provided", 400);

        const decoded = jwt.verify(refreshToken, SECRET_REFRESH_KEY);
        if (!decoded) throw new CustomError('Invalid token', 400);

        const user = await User.findOne({ where : { id: decoded.id } });

        const tokenBody = {
            id: user.id,
            phone: user.phone,
            email: user.email,
            is_admin: user.is_admin,
            name: user.name
        };

        const accessToken = this.#generateToken(tokenBody, SECRET_ACCESS_KEY);
        refreshToken = this.#generateToken(tokenBody, SECRET_REFRESH_KEY);

        const stations = await stationService.getUsersStation(user.id);

        return {
            access_token: accessToken,
            refresh_token: refreshToken,
            user: {
                id: user.id,
                email: user.email,
                stations,
                role: "admin"
            }
        };
    }

    async #hashPassword(password) {
        const saltRounds = 10;
        return await bcrypt.hash(password, saltRounds);
    }

    async #checkPassword(password, hashedPassword) {
        return await bcrypt.compare(password, hashedPassword);
    }

    #generateToken(tokenBody, key) {
        return jwt.sign(tokenBody, key);
    }
}

const userService = new UserService();

module.exports = userService;
