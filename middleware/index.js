const jwt = require('jsonwebtoken');
const { SECRET_ACCESS_KEY }= require('../config/config');

function isAuth(req, res, next) {
    const token = req.headers['authorization'];
    if (!token || !token.startsWith('Bearer ')) return res.status(401).json({message:'No token provided'});
    try {
        const decoded = jwt.verify(token.substring(7), SECRET_ACCESS_KEY);
        req.user = decoded;
        next();
    } catch (error) {
        console.log(error);
        res.status(400).json({message:'Invalid token'});
    }
}

function isAdmin(req, res, next) {
    const { user } = req;
    console.log(user);
    if (user.is_admin) return next();
    res.status(403).json({message:'No permission'});
}

module.exports = {
    isAuth,
    isAdmin
}