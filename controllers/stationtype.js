const stationTypeService = require('../services/stationtype');

class StationTypeController {
    async createStationType (req, res) {
        const { body } = req;
        try {
            const result = await stationTypeService.createStationType(body);
            res.status(201).json(result);
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: error });
        }
    }

    async getStationTypeById (req, res) {
        const {id} = req.params;
        try {
            const result = await stationTypeService.getStationTypeById(id);
            res.status(200).json(result);
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: error });
        }
    }

    async getAllStationTypes (req, res) {
        try {
            const result = await stationTypeService.getAllStationTypes();
            res.status(200).json(result);
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: error.message });
        }
    }

}

const stationTypeController = new StationTypeController();

module.exports = stationTypeController;