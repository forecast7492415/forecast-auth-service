const userService = require('../services/user');
const lg = console.log

class UserController {
    async registerUser(req, res) {
        const { body } = req;
        try {
            const result = await userService.registerUser(body, false);
            res.status(201).json(result);
        } catch(error) {
            lg(error);
            res.status(400).json({ message: error.message });
        }
    }
    async registerAdmin(req, res) {
        const { body } = req;
        try {
            const result = await userService.registerUser(body, true);
            res.status(201).json(result);
        } catch(error) {
            lg(error);
            res.status(400).json({ message: error.message });
        }
    }
    async loginUser(req, res) {
        const { body } = req;
        try {
            const result = await userService.loginUser(body, false);
            res.status(200).json(result);
        } catch(error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json({ message: error.message });
        }
    }
    async loginAdmin(req, res) {
        const { body } = req;
        try {
            const result = await userService.loginUser(body, true);
            res.status(200).json(result);
        } catch(error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json({ message: error.message });
        }
    }
    async getAccessToken(req, res) {
        const { body } = req;
        try {
            const result = await userService.getAccessToken(body);
            res.status(200).json(result);
        } catch(error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json({ message: error.message });
        }
    }
}

const userController = new UserController();

module.exports = userController;