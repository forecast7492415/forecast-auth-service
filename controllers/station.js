const stationService = require('../services/station');
const lg = console.log

class StationController {
    async createStation (req, res) {
        const { body } = req;
        try {
            const result = await stationService.createStation(body);
            res.status(201).json(result);
        } catch (error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json({ message: error });
        }
    }
    async getStationById (req, res) {
        const {id} = req.params;
        try {
            const result = await stationService.getStationById(id);
            res.status(200).json(result);
        } catch (error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json({ message: error });
        }
    }
    async addUserToStation(req, res) {
        const userId = req.query.user;
        const stationId = req.params.id;
        try {
            const result = await stationService.addUserToStation(userId, stationId);
            res.status(201).json(result);
        } catch (error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json({ message: error.message });
        }
    }
}

const stationController = new StationController();

module.exports = stationController;