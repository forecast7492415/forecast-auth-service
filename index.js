require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes');
const cors = require('cors');
const app = express();


app.use(cors({ origin: true }));
app.use(bodyParser.json());

app.use(routes);

const port = process.env.PORT;

app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
});


