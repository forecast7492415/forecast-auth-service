require('dotenv').config();
module.exports = {
    SECRET_ACCESS_KEY: "secret_access",
    SECRET_REFRESH_KEY: "secret_refres",
    "development": {
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: "postgres",
        define: {
            timestamps:true
        } 
    }
}